
/*EMAIL REGEX*/
 let regex = /^[a-zA-Z0-9\.]*@[a-zA-Z0-9]*[\.][a-zA-Z]*/;
 let tmp = tmp = regex.test('foo@bar.baz'); // true
 	 
 	 
/*BATTLE GAME*/
function attack(attackingPlayer, defendingPlayer, baseDamage, variableDamage){
	let randomVariableDamage = Math.floor(Math.random() * Math.floor(variableDamage + 1));
	let totalDamage = baseDamage + randomVariableDamage;
    defendingPlayer.health = defendingPlayer.health - totalDamage;
    if(defendingPlayer.health <= 0)
    	return `${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage} ${attackingPlayer.name} wins!`;
    else
    return `${attackingPlayer.name} hits ${defendingPlayer.name} for ${totalDamage} leaving ${defendingPlayer.health} health left`;
};

let player1 = {
    "health":10,
    "name":"zombie"
};

let player2= {
    "health":10,
    "name":"ghost"
};

console.log(attack(player1,player2,2,3)); // zombie hits ghost for 5 leaving 5 health left
console.log(attack(player2,player1,2,3)); // ghost hits zombie for 2 leaving 8 health left
console.log(attack(player1,player2,2,3)); // zombie hits ghost for 4 leaving 1 health left
console.log(attack(player2,player1,2,3)); // ghost hits zombie for 5 leaving 3 health left
console.log(attack(player1,player2,2,3)); // zombie hits ghost for 2 zombie wins!


/*SPACESHIP*/
	
function spaceShip(name, topSpeed){
    const shipName = name;
    let shipTopSpeed = topSpeed;
    this.newTopSpeed = function(speed){
        shipTopSpeed = speed;
    };
    this.accelerate = function(){
        console.log(`${this.shipName} moving to ${this.shipTopSpeed}`);
    }
};

const Enterprise = new spaceShip('Enterprise',9);
Enterprise.accelerate(); // Enterprise moves to warp 9
Enterprise.newTopSpeed(9.99);
Enterprise.accelerate(); // Enterprise moves to warp 9.99

const Prometheus = new spaceShip('Prometheus', 12);
Prometheus.accelerate(); // Prometheus moves to warp 12


/*PHONE NUMBER*/

function testPhoneNumber(number){
    const regex = /^[(][0-9]{3}[)] [0-9]{3}[-][0-9]{4}/;
    const regex2 =/^[0-9]{3}[-][0-9]{3}[-][0-9]{4}/;
    const regex3 = /^[0-9]{3} [0-9]{3} [0-9]{4}/;
	return regex.test(number) || regex2.test(number) || regex3.test(number);
};

function parsePhoneNumber(number){
    const regexA = /\d+/g;
    let areaCode = regexA.exec(number);
    let phone1 = regexA.exec(number);
    let phone2 = regexA.exec(number);
    
    let phoneReturn = {
        areaCode : areaCode[0],
        phoneNumber : phone1[0] + phone2[0]
    };
    return phoneReturn;
};

parsePhoneNumber('(206) 333-4444'); // {areaCode: "206", phoneNumber: "3334444"}

/*SOCCER STANDINGS*/
	